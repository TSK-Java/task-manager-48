package ru.tsc.kirillov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kirillov.tm.dto.request.DataBase64SaveRequest;
import ru.tsc.kirillov.tm.enumerated.Role;

public final class DataSaveBase64Command extends AbstractDataCommand {

    @NotNull
    @Override
    public String getName() {
        return "data-save-base64";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Сохранить состояние приложения в формате Base64";
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[Сохранение состояния приложения в формате Base64]");
        getDomainEndpoint().saveDataBase64(new DataBase64SaveRequest(getToken()));
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
