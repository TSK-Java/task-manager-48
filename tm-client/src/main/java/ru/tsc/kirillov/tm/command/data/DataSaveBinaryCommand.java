package ru.tsc.kirillov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kirillov.tm.dto.request.DataBinarySaveRequest;
import ru.tsc.kirillov.tm.enumerated.Role;

public final class DataSaveBinaryCommand extends AbstractDataCommand {

    @NotNull
    @Override
    public String getName() {
        return "data-save-bin";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Сохранить состояние приложения в бинарном формате";
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[Сохранение состояния приложения в бинарном формате]");
        getDomainEndpoint().saveDataBinary(new DataBinarySaveRequest(getToken()));
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
