package ru.tsc.kirillov.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.kirillov.tm.api.endpoint.*;
import ru.tsc.kirillov.tm.api.repository.IDomainRepository;
import ru.tsc.kirillov.tm.api.service.*;
import ru.tsc.kirillov.tm.api.service.dto.IProjectServiceDTO;
import ru.tsc.kirillov.tm.api.service.dto.IProjectTaskServiceDTO;
import ru.tsc.kirillov.tm.api.service.dto.ITaskServiceDTO;
import ru.tsc.kirillov.tm.api.service.dto.IUserServiceDTO;
import ru.tsc.kirillov.tm.endpoint.*;
import ru.tsc.kirillov.tm.repository.DomainRepository;
import ru.tsc.kirillov.tm.service.*;
import ru.tsc.kirillov.tm.service.dto.ProjectServiceDTO;
import ru.tsc.kirillov.tm.service.dto.ProjectTaskServiceDTO;
import ru.tsc.kirillov.tm.service.dto.TaskServiceDTO;
import ru.tsc.kirillov.tm.service.dto.UserServiceDTO;
import ru.tsc.kirillov.tm.util.SystemUtil;

import javax.xml.ws.Endpoint;
import java.io.File;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

public final class Bootstrap implements IServiceLocator {

    @NotNull
    private final IDomainRepository domainRepository = new DomainRepository(this);

    @Getter
    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @Getter
    @NotNull
    private final ITaskServiceDTO taskService = new TaskServiceDTO(connectionService);

    @Getter
    @NotNull
    private final IProjectServiceDTO projectService = new ProjectServiceDTO(connectionService);

    @Getter
    @NotNull
    private final IProjectTaskServiceDTO projectTaskService =  new ProjectTaskServiceDTO(projectService, taskService);

    @Getter
    @NotNull
    private final IUserServiceDTO userService = new UserServiceDTO(connectionService, propertyService);

    @Getter
    @NotNull
    private final ILoggerService loggerService = new LoggerService();

    @Getter
    @NotNull
    private final IAuthService authService = new AuthService(propertyService, userService);

    @Getter
    @NotNull
    private final IDomainService domainService = new DomainService(domainRepository);

    @NotNull
    private final IAuthEndpoint authEndpoint = new AuthEndpoint(this);

    @NotNull
    private final IUserEndpoint userEndpoint = new UserEndpoint(this);

    @NotNull
    private final ISystemEndpoint systemEndpoint = new SystemEndpoint(this);

    @NotNull
    private final IDomainEndpoint domainEndpoint = new DomainEndpoint(this);

    @NotNull
    private final IProjectEndpoint projectEndpoint = new ProjectEndpoint(this);

    @NotNull
    private final ITaskEndpoint taskEndpoint = new TaskEndpoint(this);

    @NotNull
    private final IProjectTaskEndpoint projectTaskEndpoint = new ProjectTaskEndpoint(this);

    @NotNull
    private final Backup backup = new Backup(this);

    {
        registry(authEndpoint);
        registry(systemEndpoint);
        registry(userEndpoint);
        registry(domainEndpoint);
        registry(projectEndpoint);
        registry(taskEndpoint);
        registry(projectTaskEndpoint);
    }

    private void registry(@NotNull final Object endpoint) {
        @NotNull final String url =
                String.format(
                        "http://%s:%s/%s?WSDL",
                        propertyService.getServerHost(),
                        propertyService.getServerPort(),
                        endpoint.getClass().getSimpleName()
                );
        Endpoint.publish(url, endpoint);
    }

    private void prepareShutdown() {
        loggerService.info("** Сервер Task Manager остановлен **");
        backup.stop();
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String fileName = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(fileName), pid.getBytes(StandardCharsets.UTF_8));
        @NotNull final File file = new File(fileName);
        file.deleteOnExit();
    }

    public void run() {
        loggerService.info("** Сервер Task Manager запущен **");
        Runtime.getRuntime().addShutdownHook(new Thread(this::prepareShutdown));
        initPID();
        backup.start();
    }

}
