package ru.tsc.kirillov.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kirillov.tm.api.repository.model.IUserRepository;
import ru.tsc.kirillov.tm.api.service.IConnectionService;
import ru.tsc.kirillov.tm.api.service.IPropertyService;
import ru.tsc.kirillov.tm.api.service.model.IUserService;
import ru.tsc.kirillov.tm.enumerated.Role;
import ru.tsc.kirillov.tm.exception.field.*;
import ru.tsc.kirillov.tm.exception.user.UserNameAlreadyExistsException;
import ru.tsc.kirillov.tm.exception.user.UserNotFoundException;
import ru.tsc.kirillov.tm.model.User;
import ru.tsc.kirillov.tm.repository.model.UserRepository;
import ru.tsc.kirillov.tm.util.HashUtil;

import javax.persistence.EntityManager;

public class UserService extends AbstractService<User, IUserRepository> implements IUserService {

    @NotNull
    private final IPropertyService propertyService;

    @NotNull
    @Override
    protected IUserRepository getRepository(@NotNull final EntityManager entityManager) {
        return new UserRepository(entityManager);
    }

    public UserService(
            @NotNull final IConnectionService connectionService,
            @NotNull final IPropertyService propertyService
    ) {
        super(connectionService);
        this.propertyService = propertyService;
    }

    @NotNull
    @Override
    public User create(@Nullable final String login, @Nullable final String password, @Nullable final String email) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        if (isLoginExists(login)) throw new UserNameAlreadyExistsException(login);
        if (isEmailExists(email)) throw new EmailAlreadyExistsException(email);
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final User user = new User();
        try {
            @NotNull final IUserRepository repository = getRepository(entityManager);
            user.setLogin(login);
            user.setPasswordHash(HashUtil.salt(propertyService, password));
            user.setEmail(email);
            entityManager.getTransaction().begin();
            repository.add(user);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return user;
    }

    @NotNull
    @Override
    public User create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email,
            @Nullable final Role role
    ) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        if (role == null) throw new RoleEmptyException();
        if (isLoginExists(login)) throw new UserNameAlreadyExistsException(login);
        if (isEmailExists(email)) throw new EmailAlreadyExistsException(email);
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final User user = new User();
        try {
            @NotNull final IUserRepository repository = getRepository(entityManager);
            user.setLogin(login);
            user.setPasswordHash(HashUtil.salt(propertyService, password));
            user.setEmail(email);
            user.setRole(role);
            entityManager.getTransaction().begin();
            repository.add(user);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return user;
    }

    @Override
    public boolean isLoginExists(@Nullable final String login) {
        if (login == null || login.isEmpty()) return false;
        return findByLogin(login) != null;
    }

    @Override
    public boolean isEmailExists(@Nullable final String email) {
        if (email == null || email.isEmpty()) return false;
        return findByEmail(email) != null;
    }

    @Nullable
    @Override
    public User findByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserRepository repository = getRepository(entityManager);
            return repository.findByLogin(login);
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public User findByEmail(@Nullable final String email) {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserRepository repository = getRepository(entityManager);
            return repository.findByEmail(email);
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public User remove(@Nullable final User model) {
        if (model == null) return null;
        @NotNull final EntityManager entityManager = getEntityManager();
        @Nullable final User user;
        try {
            @NotNull final IUserRepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            user = repository.remove(model);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return user;
    }

    @Nullable
    @Override
    public User removeByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable final User user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        return remove(user);
    }

    @Nullable
    @Override
    public User setPassword(@Nullable final String userId, @Nullable final String password) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @Nullable final User user = findOneById(userId);
        if (user == null) return null;
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        update(user);
        return user;
    }

    @Nullable
    @Override
    public User updateUser(
            @Nullable final String userId,
            @Nullable final String firstName,
            @Nullable final String lastName,
            @Nullable final String middleName
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @Nullable final User user = findOneById(userId);
        if (user == null) return null;
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        update(user);
        return user;
    }

    @Override
    public void lockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable final User user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setLocked(true);
        update(user);
    }

    @Override
    public void unlockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable final User user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setLocked(false);
        update(user);
    }

}
