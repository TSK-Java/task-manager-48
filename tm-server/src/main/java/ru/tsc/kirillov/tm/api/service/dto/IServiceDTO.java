package ru.tsc.kirillov.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kirillov.tm.api.repository.dto.IRepositoryDTO;
import ru.tsc.kirillov.tm.dto.model.AbstractModelDTO;
import ru.tsc.kirillov.tm.enumerated.Sort;

import java.util.List;

public interface IServiceDTO<M extends AbstractModelDTO> extends IRepositoryDTO<M> {

    @NotNull
    List<M> findAll(@Nullable Sort sort);

}
